﻿using UnityEngine;
using System.Collections;

public class Bateria : MonoBehaviour 
{
	private int rotateSpeed = 0;
	public GameObject baterias;
	//public static bool isBatteryPicked = false;
	// para baterias en reserva
	//public static int reserveBatteries;
	
	void Awake()
	{
		rotateSpeed = Random.Range (5, 7);
		//baterias = GameObject.FindGameObjectWithTag ("Bateria");
		GameController.reserveBatteries = 0;
	}
	
	void FixedUpdate()
	{
		transform.Rotate(new Vector3(rotateSpeed, 0, 0));
	}
	
	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.CompareTag ("Player") == true) 
		{
			// Desaparecer el item
			GameController.isBatteryPicked = true;
			// pasarlo al stock si ya está llena la batería.
			Destroy(gameObject);
		}
	}
}
