﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody))]
public class AgressiveFishMovement : MonoBehaviour
{

    public float speed;
    public GameObject player;
    public float chaseDistance;
    public float baitChaseDistance;
    public List<GameObject> targetPoints;
    public float wanderTurnTime;
    public float eatTurnTime;
    public float killPlayerDistance;
    public float minDistanceWander;
    public float minAttackDistance;

    private bool changedState;
    private bool IsMoving;
    public Enums.AgressiveFishState actualState;
    private Enums.AgressiveFishState pastState;
    private Vector3 actualTarget;
    private GameObject actualBait;
    private bool isBait;
    private bool eatBait;
    private Rigidbody fishRB;

    void OnCollisionEnter(Collision col)
    {

    }

    void OnCollisionStay(Collision col)
    {


    }
    void OnCollisionExit(Collision col)
    {
        if (col.gameObject.layer == 10)
        {
            fishRB.velocity = Vector3.zero;
            fishRB.angularVelocity = Vector3.zero;
            transform.LookAt(actualTarget);
        }

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag.Equals("Bait"))
        {
            actualState = Enums.AgressiveFishState.COMER;
            actualBait = col.gameObject;
        }

    }
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag.Equals("Bait"))
        {
            actualState = Enums.AgressiveFishState.WANDER;
             actualBait = null;
        }
    }

    void Start()
    {
        actualState = Enums.AgressiveFishState.WANDER;
        fishRB = GetComponent<Rigidbody>();
        player = GameObject.FindGameObjectWithTag("Player");
        IsMoving = false;
    }


    void Update()
    {
        
        switch (actualState)
        {
            case Enums.AgressiveFishState.WANDER:
                Wander();
                break;
            case Enums.AgressiveFishState.ATTACK:
                Attack();
                break;
            case Enums.AgressiveFishState.COMER:
                Comer();
                break;
            default:
                break;
        }

    }

    private void Attack()
    {
        transform.LookAt(player.transform.position);
        actualTarget = player.transform.position;
        if (!IsMoving)
        {
            StartCoroutine(Movement(actualState, killPlayerDistance));

        }
        else
        {
            if (Vector3.Distance(transform.position, player.transform.position) < killPlayerDistance)
            {
                GameController.actualGameState = Enums.GameStates.DEATH;
            }

            if (Vector3.Distance(transform.position, player.transform.position) > chaseDistance)
            {
                actualState = Enums.AgressiveFishState.WANDER;
            }

        }
    }

    private void Wander()
    {
        if (targetPoints.Count > 0)
        {

            if (!IsMoving)
            {
                actualTarget = targetPoints[Random.Range(0, targetPoints.Count)].transform.position;

                if (Vector3.Distance(transform.position, actualTarget) > 0.1f)
                {
                    StartCoroutine(Movement(actualState, minDistanceWander));
                }
            }
            else
            {
                CheckForPlayer();

            }

        }

    }

    private void Comer()
    {
        if (!eatBait)
        {

            if (actualBait != null)
            {
                actualTarget = actualBait.transform.position;
                StartCoroutine(EatBait());
            }
            else
            {
                eatBait = true;
            }

        }
        else
        {
            fishRB.velocity = Vector3.zero;
            StopCoroutine(EatBait());
            Destroy(actualBait);
            actualBait = null;
            actualState = Enums.AgressiveFishState.WANDER;
            eatBait = false;
        }
    }


    private void CheckForPlayer()
    {
        if (Vector3.Distance(transform.position, player.transform.position) <= chaseDistance)
        {
            actualState = Enums.AgressiveFishState.ATTACK;
        }

    }


    IEnumerator Movement(Enums.AgressiveFishState state, float minDistance)
    {
        IsMoving = true;
        float t = 0.0f;
        t = Time.deltaTime * speed;
        float time = wanderTurnTime;
        var originalTime = time;
        var lookPos = actualTarget - transform.position;
        var rotation = Quaternion.LookRotation(lookPos);
        Quaternion initialRotation = transform.rotation;

        //Rotate to the target
        while (time > 0.0f)
        {
            time -= Time.deltaTime;
            transform.rotation = Quaternion.Lerp(initialRotation, rotation, 1 - (time / originalTime));
            yield return null;
        }


        while (Vector3.Distance(transform.position, actualTarget) > minDistance)
        {
            fishRB.AddForce(transform.forward * t);
            yield return null;
            if (state != actualState)
            {
                IsMoving = false;
                fishRB.velocity = Vector3.zero;
                yield break;
            }
        }
        IsMoving = false;
        fishRB.velocity = Vector3.zero;
        yield break;
    }

    IEnumerator EatBait()
    {

        eatBait = false;
        float t = 0.0f;
        t = Time.deltaTime * speed;
        if (actualBait != null)
        {
            float time = eatTurnTime;
            var originalTime = time;
            var lookPos = actualTarget - transform.position;
            
            var rotation = Quaternion.LookRotation(lookPos);
            Quaternion initialRotation = transform.rotation;

            //Rotate to the target
            while (time > 0.0f)
            {
                time -= Time.deltaTime;
                transform.rotation = Quaternion.Lerp(initialRotation, rotation, 1 - (time / originalTime));
                yield return null;
            }


            while (Vector3.Distance(transform.position, actualTarget) > 0.2)
            {
                if (actualBait == null)
                {
                    break;
                }
                fishRB.AddForce(transform.forward * t);
                yield return null;
            }
            eatBait = true;
        }
        yield break;
    }


}
