﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Entities;
using UnityEngine.UI;

public class BookInventoryManager : MonoBehaviour
{

    #region Propierties

    public List<GameObject> inventoryPoints;
    public Vector3 itemRotation;
    public float rotationPercentage;
    public Text nameText;
    public Text descriptionText;

    private static List<GameObject> Points = new List<GameObject>();
    private static List<Item> picketItems = new List<Item>();
    private int actualIndex = -1;

    #endregion


    #region Events

    void Awake()
    {
        MergePoints();
    }

    // Use this for initialization
    void Start()
    {

    }


    // Update is called once per frame
    void Update()
    {
        CheckInput();
        if (!GameController.IsInventaryOpen)
        {
            actualIndex = 0;
            RefreshItemsPosition();

        }
        RefreshUI();
    }

    #endregion

    #region Methods


    private void RefreshUI()
    {
        if (GameController.CameraActualState == Enums.CameraStates.BOOKINVENTORY)
        {

            nameText.text = GameController.ReadeableItems[actualIndex].ItemName;
            descriptionText.text = GameController.ReadeableItems[actualIndex].ItemDescription;
        }

    }

    public static void SaveInInventory(GameObject item)
    {
        GameObject point = Points[GameController.ReadeableItems.Count - 1];
        item.transform.position = point.transform.position;
        item.transform.localRotation = Quaternion.Euler(282, 0, 0);
        item.transform.SetParent(point.transform);

    }

    private void MergePoints()
    {

        foreach (var item in inventoryPoints)
        {
            Points.Add(item);
        }
    }

    private void CloseInventory()
    {
        GameController.CameraActualState = Enums.CameraStates.PLAYER;
        GameController.freezePlayerMovement = GameController.IsInventaryOpen;
        GameController.freezeCameraMovement = GameController.IsInventaryOpen;
    }

    private void CloseBookInventory()
    {
        GameController.CameraActualState = Enums.CameraStates.GENERALINVENTORY;

    }

    void CheckInput()
    {
        if (GameController.CameraActualState == Enums.CameraStates.BOOKINVENTORY)
        {



            if (Input.GetButtonDown(GameKeysStrings.Inputs.Inventary) && GameController.IsInventaryOpen)
            {
                CloseInventory();

            }
            if (Input.GetButtonDown(GameKeysStrings.Inputs.Cancel) && GameController.IsInventaryOpen)
            {
                CloseBookInventory();

            }

            if (GameController.CameraActualState == Enums.CameraStates.BOOKINVENTORY && GameController.InventoryItems.Count > 0)
            {

                if (Input.GetButtonDown(GameKeysStrings.Inputs.Right))
                {

                    MoveForward();
                }

                if (Input.GetButtonDown(GameKeysStrings.Inputs.Left))
                {
                    MoveBackward();
                }
                if (Input.GetButtonDown(GameKeysStrings.Inputs.Action))
                {

                }
            }
        }
    }

    private void MoveForward()
    {
        float difference = ((float)actualIndex + 1) / ((float)GameController.ReadeableItems.Count - 1);
        if (difference > 1)
            actualIndex = 0;
        else
            actualIndex++;

        float finalRotation = actualIndex * -rotationPercentage;
        Quaternion rot = Quaternion.Euler(Vector3.right * finalRotation);
        transform.localRotation = rot;

    }

    private void MoveBackward()
    {
        float difference = ((float)actualIndex - 1) / ((float)GameController.ReadeableItems.Count - 1);
        if (difference < 0)
            actualIndex = GameController.ReadeableItems.Count - 1;
        else
            actualIndex--;

        float finalRotation = actualIndex * -rotationPercentage;
        Quaternion rot = Quaternion.Euler(Vector3.right * finalRotation);
        transform.localRotation = rot;
    }

    private void RefreshItemsPosition()
    {

        for (int i = 0; i < GameController.ReadeableItems.Count; i++)
        {
            GameController.ReadeableItems[i].ItemGO.transform.position = Points[i].transform.position;
            GameController.ReadeableItems[i].ItemGO.transform.SetParent(Points[i].transform);
        }

    }
    #endregion



}
