﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OxigenTank : MonoBehaviour
{


    public Slider sliderLung;
    public Slider sliderTank;

    public float tankLevel;
    private float lungLevel = 30.0f;
    private float relationLevel = 10.0f;
    private float oxigenComsumption = 0.025f;

    private bool oxigenIsFromTank = false;
    private bool isComsuming = false;
    private bool isInWater = false;

    private void consumeOxigen()
    {

        if (isComsuming)
        {
            if (oxigenIsFromTank)
            {
                if (!consumeOxigenFromTank())
                {
                    if (isInWater)
                    {
                        if (!consumeOxigenFromLung())
                        {
                            GameController.actualGameState = Enums.GameStates.DEATH;
                            
                            //Debug.Log ("Muerto");
                        }
                    }
                }
            }
            else
            {
                if (isInWater)
                {
                    if (!consumeOxigenFromLung())
                    {
                        GameController.actualGameState = Enums.GameStates.DEATH;
                        
                        //Debug.Log ("Muerto");
                    }
                }
            }
        }

        updateConsole();
    }

    private bool consumeOxigenFromTank()
    {
        if (tankLevel > 0)
        {
            tankLevel = tankLevel - oxigenComsumption;
            return true;
        }

        return false;
    }

    private bool consumeOxigenFromLung()
    {
        if (lungLevel > 0)
        {
            lungLevel = lungLevel - oxigenComsumption;
            return true;
        }

        return false;
    }

    private void updateConsole()
    {
        string info;

        info = "Tank: " + tankLevel + " - ";
        info += "Lung: " + lungLevel + "\n" + (isInWater ? "Under" : "Above");
        if (sliderLung != null)
        {
            sliderLung.value = lungLevel;
        }
        if (sliderTank != null)
        {
            sliderTank.value = tankLevel;
        }


        //console.text = info;
        //Debug.Log (info);
    }

    // Use this for initialization
    void Start()
    {
        GameController.IsDeath = false;
        oxigenComsumption = 1f * Time.deltaTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameController.IsInWater == true)
        {
            isInWater = true;
            isComsuming = true;
            if (tankLevel <= 0)
            {
                oxigenIsFromTank = false;
            }
        }
        else
        {
            isInWater = false;
            if (!oxigenIsFromTank)
            {
                lungLevel = 30.0f;
                isComsuming = false;
            }
        }

        if (Input.GetButtonDown("Tank"))
        {
            oxigenIsFromTank = !oxigenIsFromTank;
            if (oxigenIsFromTank)
            {
                isComsuming = true;
            }
            else if (!isInWater)
            {
                isComsuming = false;
            }
        }

        consumeOxigen();


        /*
        if (Input.GetKeyDown (KeyCode.DownArrow)) {
            isInWater = true;
            isComsuming = true;
            if(tankLevel <= 0) {
                oxigenIsFromTank = false;
            }
        }

        if (Input.GetKeyDown (KeyCode.UpArrow)) {
            isInWater = false;
            if(!oxigenIsFromTank) {
                lungLevel = 30.0f;
                isComsuming = false;
            }
        } 



        if (Input.GetKeyDown (KeyCode.A)) {
            float changeOxigen = relationLevel;
            if(tankLevel < relationLevel) {
                changeOxigen = tankLevel;
            }

            tankLevel -= changeOxigen;
            lungLevel += changeOxigen;
        }
        */
    }
}
