﻿using UnityEngine;
using System.Collections;

public class Asustarse : MonoBehaviour 
{
	public GameObject enemyCube;
	
	private Vector3 miForward;
	private Vector3 distEnemy;
	private float distEnemyX;
	private float distEnemyY;
	private float distEnemyZ;
	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		// Hacer el cono de visión
		miForward = transform.forward;
		distEnemy = enemyCube.transform.position - transform.position;
		distEnemyX = Mathf.Abs(distEnemy.x);
		distEnemyY = Mathf.Abs(distEnemy.y);
		distEnemyZ = Mathf.Abs(distEnemy.z);
		
		GameController.angleEnemy  = Vector3.Angle (miForward, distEnemy);
		GameController.avDistEnemy = (distEnemyX + distEnemyY + distEnemyZ) / 3;
		
		
		// Asustarse con luz apagada o prendida
		if (GameController.lightOn == false) 
		{
			if ((Mathf.Abs(GameController.avDistEnemy) < 3))
			{
				//Debug.Log ("Me asusto un poco sin luz");
				GameController.fearLevel += 5;
			}
		}
//		else if (GameController.lightOn == true) 
//		{
//			if ((Mathf.Abs(GameController.avDistEnemy) >= 0) && (Mathf.Abs(GameController.avDistEnemy) <= 2)) 
//			{
//				if ((GameController.angleEnemy >= 0) && (GameController.angleEnemy <= GameController.flashLight.spotAngle/2))
//				{
//					//Debug.Log ("QUE SUSTO!");
//				}
//				else if ((GameController.angleEnemy > GameController.flashLight.spotAngle/2) && (GameController.angleEnemy <= GameController.flashLight.spotAngle))
//				{
//					//Debug.Log ("Me asusto un poco con luz");
//				}
//			}
//			else if ((Mathf.Abs(GameController.avDistEnemy) > 2) && (Mathf.Abs(GameController.avDistEnemy) <= 8))
//			{
//				if ((GameController.angleEnemy >= 0) && (GameController.angleEnemy <= GameController.flashLight.spotAngle/2))
//				{
//					//Debug.Log ("QUE SUSTO!");
//				}
//				else if ((GameController.angleEnemy > GameController.flashLight.spotAngle/2) && (GameController.angleEnemy <= GameController.flashLight.spotAngle*4/5))
//				{
//					//Debug.Log ("Me asusto un poco con luz");
//				}
//			}
//				
//		}		
	}
}
