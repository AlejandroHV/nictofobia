﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Entities;

public class MenuEvents : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public void RestartLevel()
    {
        Application.LoadLevel(GameController.ActualLevel);
    
    }

    public void Continuar()
    {

        GameController.actualGameState = Enums.GameStates.PLAYING;
        
    }

    public void LoadMainMenu()
    {
        Application.LoadLevel(GameKeysStrings.Levels.MainMenu);
    
    
    }


    public void CloseGame()
    {
        Application.Quit();
    
    }
}
