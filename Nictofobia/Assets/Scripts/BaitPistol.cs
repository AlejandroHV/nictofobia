﻿using UnityEngine;
using System.Collections;

public class BaitPistol : MonoBehaviour {

    public float bulletMultiplier;
    public GameObject bulletPrefab;
    public Transform bulletSpawnPoint;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (GameController.ActualItemInHand != null && GameController.ActualItemInHand.ItemEnum == Enums.PickeableItemsTag.Weapon)
        {
            if (Input.GetMouseButtonDown(0))
            {
                GameObject Bullet = Instantiate(bulletPrefab);
                Bullet.transform.position = bulletSpawnPoint.position;
                Rigidbody bulletRB = Bullet.GetComponent<Rigidbody>();
                float bulletFoceMultipler = bulletMultiplier * 100;
                Vector3 fireForce = bulletSpawnPoint.transform.forward * (bulletFoceMultipler * Time.deltaTime);
                bulletRB.AddForce(fireForce, ForceMode.Impulse);

            }

        }
	}
}
