﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody))]
public class FriendlyFishMovement : MonoBehaviour
{


    public float speed;
    public GameObject player;
    public float runAwayDistance;
    public float baitChaseDistance;
    public List<GameObject> targetPoints;
    public float safeDistance;
    public float avoidSpeed;
    public float wanderTurnTime;
    public float eatTurnTime;
    public float runTurnTime;

    private Rigidbody fishRB;
    private bool changedState;
    private bool IsMoving;
    public Enums.FriendlyFishState actualState;
    private Vector3 actualTarget;
    private GameObject actualBait;
    private bool isBait;
    private bool eatBait;

    void OnCollisionEnter(Collision col)
    {


    }

    void OnCollisionStay(Collision col)
    {

    }
    void OnCollisionExit(Collision col)
    {
        if (col.gameObject.layer == 10)
        {
            fishRB.velocity = Vector3.zero;
            fishRB.angularVelocity = Vector3.zero;
            transform.LookAt(actualTarget);
        }

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag.Equals("Bait"))
        {
            actualState = Enums.FriendlyFishState.EAT;
            actualBait = col.gameObject;
        }

    }
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag.Equals("Bait"))
        {
            actualState = Enums.FriendlyFishState.WANDER;
            actualBait = null;
        }
    }

    void Start()
    {
        fishRB = GetComponent<Rigidbody>();
        actualState = Enums.FriendlyFishState.WANDER;
        player = GameObject.FindGameObjectWithTag("Player");
        IsMoving = false;
    }


    void Update()
    {
        
        switch (actualState)
        {
            case Enums.FriendlyFishState.WANDER:
                Wander();
                break;
            case Enums.FriendlyFishState.RUNAWAY:
                RunAway();
                break;
            case Enums.FriendlyFishState.EAT:
                Comer();
                break;
            default:
                break;
        }

    }

    private void RunAway()
    {

        if (!IsMoving)
        {
            StartCoroutine(Avoid());

        }
        else
        {
            if (Vector3.Distance(transform.position, player.transform.position) > safeDistance)
            {
                fishRB.velocity = Vector3.zero;
                actualState = Enums.FriendlyFishState.WANDER;
            }

        }
    }

    private void Wander()
    {
        if (targetPoints.Count > 0)
        {

            if (!IsMoving)
            {
                actualTarget = targetPoints[Random.Range(0, targetPoints.Count)].transform.position;

                if (Vector3.Distance(transform.position, actualTarget) > 0.1f)
                {
                    StartCoroutine(Movement(actualState));
                }
            }
            else
            {
                CheckForPlayer();

            }

        }

    }

    private void Comer()
    {
        if (!eatBait)
        {

            if (actualBait != null)
            {
                actualTarget = actualBait.transform.position;
                StartCoroutine(EatBait());
            }
            else
            {
                eatBait = true;
            }

        }
        else
        {
            fishRB.velocity = Vector3.zero;
            StopCoroutine(EatBait());
            Destroy(actualBait);
            actualBait = null;
            actualState = Enums.FriendlyFishState.WANDER;
            eatBait = false;
        }
    }


    private void CheckForPlayer()
    {
        if (Vector3.Distance(transform.position, player.transform.position) <= runAwayDistance)
        {
            actualState = Enums.FriendlyFishState.RUNAWAY;
            fishRB.velocity = Vector3.zero;
        }
    }

    IEnumerator Avoid()
    {
        IsMoving = true;
        transform.Rotate(new Vector3(0, 1, 0), Random.Range(90, 270), Space.Self);
        float t = 0.0f;
        t = Time.deltaTime * avoidSpeed;
        while (Vector3.Distance(transform.position, player.transform.position) < safeDistance)
        {
            fishRB.AddForce(transform.forward * t);
            yield return null;
        }
        IsMoving = false;
        fishRB.velocity = Vector3.zero;
        yield break;

    }

    IEnumerator Movement(Enums.FriendlyFishState state)
    {
        IsMoving = true;
        float t = 0.0f;
        t = Time.deltaTime * speed;
        float time = wanderTurnTime;
        var originalTime = time;
        var lookPos = actualTarget - transform.position;
        var rotation = Quaternion.LookRotation(lookPos);
        Quaternion initialRotation = transform.rotation;

        //Rotate to the target
        while (time > 0.0f)
        {
            time -= Time.deltaTime;
            transform.rotation = Quaternion.Lerp(initialRotation, rotation, 1 - (time / originalTime));
            yield return null;
        }


        while (Vector3.Distance(transform.position, actualTarget) > 5)
        {
            fishRB.AddForce(transform.forward * t);
            yield return null;
            if (state != actualState)
            {
                IsMoving = false;
                fishRB.velocity = Vector3.zero;
                yield break;
            }
        }
        IsMoving = false;
        fishRB.velocity = Vector3.zero;
        yield break;
    }

    IEnumerator EatBait()
    {

        eatBait = false;
        float t = 0.0f;
        t = Time.deltaTime * speed;
        if (actualBait != null)
        {
            float time = eatTurnTime;
            var originalTime = time;
            var lookPos = actualTarget - transform.position;
            var rotation = Quaternion.LookRotation(lookPos);
            Quaternion initialRotation = transform.rotation;

            //Rotate to the target
            while (time > 0.0f)
            {
                time -= Time.deltaTime;
                transform.rotation = Quaternion.Lerp(initialRotation, rotation, 1 - (time / originalTime));
                yield return null;
            }


            while (Vector3.Distance(transform.position, actualTarget) > 0.2)
            {
                if (actualBait == null)
                {
                    break;
                }
                fishRB.AddForce(transform.forward * t);
                //transform.position = Vector3.MoveTowards(transform.position, actualTarget, t);
                yield return null;
            }
            eatBait = true;
        }
        yield break;
    }

}
