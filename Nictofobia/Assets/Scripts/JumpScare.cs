﻿using UnityEngine;
using System.Collections;

public class JumpScare : MonoBehaviour
{


    public GameObject scareObject;
    public AudioClip jumpScare;
    public float delay;
    public float EndDelay;
    public bool finalJumpScare;
    public bool singleJumpScare;
    private AudioSource source;
    private float actualCount;
    private float endCount;
    private bool show;
    // Use this for initialization

    void Awake()
    {
        actualCount =0;
        
        source = gameObject.GetComponent<AudioSource>();
        if (delay == 0.0)
        {
            delay = 30f;
        }
    }

    void Start()
    {
        
        var rendererElements = scareObject.GetComponentsInChildren<SkinnedMeshRenderer>();
        foreach (var render in rendererElements)
        {
            render.enabled = false;
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        show = false;
        source.PlayOneShot(jumpScare);
        var rendererElements = scareObject.GetComponentsInChildren<SkinnedMeshRenderer>();
        foreach (var render in rendererElements)
        {
            render.enabled = true;
        }
        //Object disapear after a count.
        if (!singleJumpScare)
        {
            for (int i = 0; i <= delay; i++)
            {
                actualCount += 1;
            }
            if(actualCount >= delay)
            {
               
                foreach (var render in rendererElements)
                {
                    render.enabled = false;
                }
            }
        }
        if (finalJumpScare)
        {
           StartCoroutine( ShowObject());
           
        }
        
            
        
       
    }

    IEnumerator ShowObject()
    { 
    
       yield return new WaitForSeconds(5f);
       show = true;
       
    }

    // Update is called once per frame
    void Update()
    {
        if (show)
        {
            GameController.actualGameState = Enums.GameStates.WON;
        }

        Debug.Log(show);
        
    }
}
