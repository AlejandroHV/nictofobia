﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Entities;

public class MainMenuController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Cursor.lockState = CursorLockMode.None;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void Quit()
    {
        Application.Quit();
    
    }

    public void StartGame()
    {
        Application.LoadLevel(GameKeysStrings.Levels.InicialCueva);
    
    }
}
