﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using Assets.Scripts.Entities;


public class GameController : MonoBehaviour
{
    public static List<string> GroundLayerTags = new List<string>(new string[] { "Ground", "Cavern" });
    public static bool IsInWater;
    public static int ActualLevel;
    public static bool IsDeath;
    public static bool HasWon;
    public static bool IsOnScapeMenu;
    public static bool freezePlayerMovement = false;
    public static bool freezeCameraMovement = false;
    public static List<Item> InventoryItems = new List<Item>();
    public static List<Item> ReadeableItems = new List<Item>();
    public static CursorLockMode LookCursorState = CursorLockMode.Locked;
    public static Enums.CameraStates CameraActualState = Enums.CameraStates.PLAYER;
    public static Enums.GameStates actualGameState = Enums.GameStates.PLAYING;
    public bool _showInteraction;
    public static bool ShowInteraction;




    public static Item ActualItemInHand = null;
    public static bool IsInventaryOpen = false;

    public GameObject canvasUI;
    public GameObject canvasDeath;
    public GameObject canvasInventory;
    public GameObject canvasScapeMenu;
    public GameObject canvasHasWon;

    public Text text;

    public static bool isBatteryPicked = false;
    public static int reserveBatteries;
    public static float batterie;
    public static bool lightOn;
    public static Light flashLight;


    // Nervios y Oxígeno (provisional)
    public static float tanqueOxigeno;
    public static float fearLevel;

    public static float angleEnemy;
    public static float avDistEnemy;

    #region Events

    void Start()
    {
        ActualLevel = Application.loadedLevel;
        ShowInteraction = _showInteraction;
        if (canvasDeath != null)
            canvasDeath.SetActive(false);

        if (text != null)
            text.enabled = false;

        if (canvasHasWon != null)
        {
            canvasHasWon.SetActive(false);
        }
        if (canvasScapeMenu != null)
        {
            canvasScapeMenu.SetActive(false);
        }
        if (canvasInventory != null)
        {
            canvasInventory.SetActive(false);
        }

        if (!ShowInteraction)
        {
            canvasDeath.SetActive(false);
            canvasUI.SetActive(false);
        }

    }



    void Update()
    {
        CheckInput();
        CheckCanvasStatus();
    }

    #endregion

    #region Methods
    public static void CursorLockState()
    {

        Cursor.lockState = LookCursorState;
    }

    public static Enums.PickeableItemsTag GetPickeableString(string objectTag)
    {
        Enums.PickeableItemsTag value = Enums.PickeableItemsTag.None;
        switch (objectTag)
        {
            case "Lantern":
                value = Enums.PickeableItemsTag.Lantern;
                break;
            case "Pistol":
                value = Enums.PickeableItemsTag.Pistol;
                break;
            default:
                value = Enums.PickeableItemsTag.None;
                break;
        }
        return value;

    }


    void CheckCanvasStatus()
    {


        switch (actualGameState)
        {
            case Enums.GameStates.PLAYING:
                freezeCameraMovement = false;
                freezePlayerMovement = false;
                canvasUI.SetActive(true);
                canvasScapeMenu.SetActive(false);
                canvasInventory.SetActive(false);
                break;
            case Enums.GameStates.INVENTORY:
                freezeCameraMovement = true;
                freezePlayerMovement = true;
                canvasInventory.SetActive(true);
                canvasUI.SetActive(false);
                break;
            case Enums.GameStates.WON:
                freezeCameraMovement = true;
                freezePlayerMovement = true;
                canvasHasWon.SetActive(true);
                canvasUI.SetActive(false);
                break;
            case Enums.GameStates.DEATH:
                Cursor.lockState = CursorLockMode.None;
                freezeCameraMovement = true;
                freezePlayerMovement = true;
                if (canvasDeath != null)
                    canvasDeath.SetActive(true);

                if (canvasUI != null)
                    canvasUI.SetActive(false);

                if (text != null)
                    text.enabled = true;
                break;
            case Enums.GameStates.SCAPE:
                freezeCameraMovement = true;
                freezePlayerMovement = true;
                Cursor.lockState = CursorLockMode.None;
                canvasScapeMenu.SetActive(true);
                canvasUI.SetActive(false);
                break;
            default:
                break;
        }
    }


    void CheckInput()
    {
        if (!IsInventaryOpen)
        {


            if (IsOnScapeMenu)
            {
                if (Input.GetButtonDown(GameKeysStrings.Inputs.Cancel))
                {
                    actualGameState = Enums.GameStates.PLAYING;

                }
            }
            else
            {
                if (Input.GetButtonDown(GameKeysStrings.Inputs.Cancel))
                {
                    actualGameState = Enums.GameStates.SCAPE;

                }


            }
        }
    }

    #endregion


}
