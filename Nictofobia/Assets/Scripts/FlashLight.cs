﻿using UnityEngine;
using System.Collections;


public class FlashLight : MonoBehaviour
{	
	//variables
	[Range(3f,6f)] 
	public float maxLight;
	[Range(0f,0.3f)] 
	public float minLight;
	// public static float batterie;		// así lo pueden acceder otros scripts
	float intensidadLuz;
	public float velDescarga;
	// recargas HOLD
	private float velRecarga_Hold;
	// recargas TAP
	private float velRecarga_Tap_Off;
	private float velRecarga_Tap_On;
	// recargas PICK-UP	
	private float velRecarga_PickUp;
	
	
	void Awake()
	{
		// linterna
		GameController.flashLight = GetComponent <Light>();
	}
	
	void Start ()
	{
		
		// Initializa variables
		
		GameController.batterie = 1f;
		velDescarga *= Time.deltaTime; 
		velRecarga_Hold = 0.15f * Time.deltaTime;
		velRecarga_Tap_Off = 0.15f * Time.deltaTime;
		velRecarga_Tap_On = 0.05f;
				
		// Empezar con la linterna apagada
		GameController.lightOn = false;		
	}
	
	// Update is called once per frame
	void Update ()
	{
		velRecarga_PickUp = 1f - GameController.batterie;
		intensidadLuz = GameController.batterie * maxLight;		
		// Prender y apagar la linterna
		if (Input.GetKeyDown(KeyCode.F))
		{
			GameController.lightOn = !GameController.lightOn; 
			//Debug.Log ("Light is now " + GameController.lightOn);
		}
		
		// Cuando está apagada
		if (!GameController.lightOn)
		{
			// la luz está apagada
			EstaApagada ();
			// Recargar mateniendo presionado R
			if (Input.GetKey(KeyCode.R))
			{
				Recharge_Hold ();
			}
			// Recargar con Tap
			if (Input.GetKeyUp(KeyCode.Plus))
			{
				Recharge_Tap ();
			}
			// Almacenar baterias recogidas y recargar con ellas
			if (GameController.isBatteryPicked == true)
			{
				GameController.reserveBatteries ++;
				GameController.isBatteryPicked = false;
				//Debug.Log ("Batterie picked up");
				//Debug.Log ("Batteries in stock: " + GameController.reserveBatteries);
			}
			if (GameController.reserveBatteries > 0) 
			{
				if ((GameController.batterie < 1) && (Input.GetKeyDown(KeyCode.B)))
				{
					Recharge_PickUp ();	
				}			
			}
		} 
		// Cuando está prendida
		else if (GameController.lightOn) 
		{
			//la luz está prendida y se descarga
			EstaPrendida ();
			Descargando ();	
			// Se puede recargar manteniendo presionado R pero se apaga la luz
			if (Input.GetKey(KeyCode.R))
			{
				GameController.lightOn = false ;
				Recharge_Hold ();
			}
			// recargar con Tap
			if (Input.GetKeyUp(KeyCode.Plus))
			{
				Recharge_Tap ();
			}
			// Almacenar baterias recogidas y recargar con ellas
			if (GameController.isBatteryPicked == true)
			{
				GameController.reserveBatteries ++;
				GameController.isBatteryPicked = false;
				//Debug.Log ("Batterie picked up");
				//Debug.Log ("Batteries in stock: " + GameController.reserveBatteries);
			}
			if (GameController.reserveBatteries > 0) 
			{
				if ((GameController.batterie < 1) && (Input.GetKeyDown(KeyCode.B)))
				{
					Recharge_PickUp ();	
				}			
			}			
		}
	}
	
	
	void EstaPrendida ()
	{
		GameController.flashLight.intensity = intensidadLuz;      	
	}
	
	void EstaApagada ()
	{
		GameController.flashLight.intensity = 0; 
	}
	
	void Descargando ()
	{
		// la batería se gasta con el uso
		GameController.batterie -= velDescarga;
		if (GameController.batterie > minLight) {
			GameController.batterie -= velDescarga;
		}
		else if (GameController.batterie <= minLight) {
			GameController.lightOn = false; 
		}
		Debug.Log ("velocidad de descarga: " + velDescarga);
	}
	
	void Cargando () 
	{
		if (GameController.batterie <= 1) 
		{
			GameController.batterie += velRecarga_Hold;
		}
	
	}
	
	void Recharge_Hold ()
	{
		if (GameController.batterie <= 1) 
		{
			GameController.batterie += velRecarga_Hold;
		}
		Debug.Log ("vel de carga con Hold es de: " + velRecarga_Hold);
	}
	
	void Recharge_Tap () 
	{
		if (GameController.batterie <= 1) 
		{
			if (GameController.lightOn == true) 
			{
				// Se recarga más rápido 
				GameController.batterie += velRecarga_Tap_On;
				Debug.Log ("La vel de recarga prendida es de: " + velRecarga_Tap_On);
			}
			else if (GameController.lightOn == false) 
			{
				// Se recarga más lento
				velRecarga_Tap_Off = 0.15f * Time.deltaTime;
				GameController.batterie += velRecarga_Tap_Off;
				Debug.Log ("La vel de recarga apagada es de: " + velRecarga_Tap_Off);
			}
		}
	}
	
	void Recharge_PickUp () 
	{	
		GameController.batterie += velRecarga_PickUp;
		GameController.reserveBatteries --;
		Debug.Log ("La vel de recarga apagada es de: " + velRecarga_PickUp);
	}	
	 	
}

