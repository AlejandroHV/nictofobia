﻿using UnityEngine;
using System.Collections;

public class ElipseMiedo : MonoBehaviour {

	public GameObject elipseMiedo;
	
	void Update ()
	{
		// hacerlo invicible
		GetComponent<MeshRenderer>().enabled = false; 
	}
	
	public void OnTriggerEnter(Collider col)
	{
		if (GameController.lightOn == false) 
		{
			if (col.gameObject.CompareTag ("EnemyFish") == true) 
			{
				Debug.Log ("El bicho está cerca");
				GameController.fearLevel +=10;
			}
		}
		else if (GameController.lightOn == true)
		{
			if (col.gameObject.CompareTag ("EnemyFish") == true) 
			{
				Debug.Log ("El bicho está cerca y lo veo!");
				GameController.fearLevel +=20;
			}
		}
	}
}
