﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public GameObject playerHead;
    public float cameraMovementPercentage;
    public float _camSpeedX = 150f;
    public float _camSpeedY = 100f;
    public Transform inventoryCameraPosition;
    public Transform bookInventoryCameraPosition;

    private Quaternion inGameCameraRotation;
    private Vector3 _mouseDelta;
    private bool isCameraInventary;
    private Vector3 playerPosition;

    // Use this for initialization
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameController.freezeCameraMovement)
        {
            MovePosition();
            ChangeRotation();
        }

        switch (GameController.CameraActualState)
        {
            case Enums.CameraStates.PLAYER:
                break;
            case Enums.CameraStates.GENERALINVENTORY:
                if (GameController.IsInventaryOpen)
                
                    ShowInventory();
                
                else
                    HideInventory();
                
                break;
            case Enums.CameraStates.BOOKINVENTORY:
                if (GameController.IsInventaryOpen)

                    ShowBookInventory();

                else
                    HideBookInventory();
                break;
            default:
                break;
        }
        

    }


    public void ShowInventory()
    {
        if (!isCameraInventary)
        {

            inGameCameraRotation = transform.localRotation;
            gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, inventoryCameraPosition.position, 10 * Time.deltaTime);
            gameObject.transform.localRotation = inventoryCameraPosition.localRotation;

        }

    }
    public void HideInventory()
    {
        if (isCameraInventary)
        {
            gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, playerHead.transform.position, 10 * Time.deltaTime);
            transform.localRotation = inGameCameraRotation;
        }

    }
    public void ShowBookInventory()
    {
        if (!isCameraInventary)
        {
            gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, bookInventoryCameraPosition.position, 10 * Time.deltaTime);
            gameObject.transform.localRotation = inventoryCameraPosition.localRotation;

        }

    }
    public void HideBookInventory()
    {
        if (isCameraInventary)
        {
            gameObject.transform.position = Vector3.Lerp(gameObject.transform.position, playerHead.transform.position, 10 * Time.deltaTime);
            transform.localRotation = inGameCameraRotation;
        }

    }


    
    private void MovePosition()
    {
        Vector3 newPosition =  playerHead.transform.position;

        //newPosition.z +=1;
        transform.position = newPosition;

    }

    private void ChangeRotation()
    {

        Vector3 finalRotation = transform.rotation.eulerAngles;
        _mouseDelta = new Vector3(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0);


        Vector3 camRotation = new Vector3(_mouseDelta.x * _camSpeedX, _mouseDelta.y * _camSpeedY, 0);
        camRotation *= Time.deltaTime;
        finalRotation += camRotation;
        finalRotation.z = 0;

        if (finalRotation.x > 270)
            finalRotation.x = finalRotation.x < 280 ? 280 : finalRotation.x;
        else if (finalRotation.x < 90)
            finalRotation.x = finalRotation.x > 80 ? 80 : finalRotation.x;

        transform.rotation = Quaternion.Euler(finalRotation);

    }
}
