﻿using UnityEngine;
using System.Collections;

public class CheckWaterState : MonoBehaviour
{
    public GameObject player;

    public MonoBehaviour script;
    
    private Rigidbody playerRigidBody;

    void OnTriggerStay(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Water"))
        {
            GameController.IsInWater = true;


        }

    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.tag.Equals("Water"))
        {
            GameController.IsInWater = false;
            playerRigidBody.AddForce(new Vector3(0,0,0));
    
        }
    }

    private void CheckWater()
    {
        if (GameController.IsInWater && !GameController.IsInventaryOpen)
        {
            RenderSettings.fog = true;
            //RenderSettings.fogColor = new Color(0.125f, 0.199f, 0.242f, 0.1f);
            //RenderSettings.fogColor = new Color(0.13f, 0.41f, 0.60f, 0f);
            RenderSettings.fogColor = new Color(0f, 0f, 0f, 1f);
            RenderSettings.fogDensity = 0.04f;
            script.enabled = true;
        }
        else
        {
            script.enabled = false;
            RenderSettings.fog = false;

        }
    
    
    }



    // Use this for initialization
    void Start()
    {
        script.enabled = false;
        playerRigidBody = player.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        

        CheckWater();
    }
}
