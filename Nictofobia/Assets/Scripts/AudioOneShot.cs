﻿using UnityEngine;
using System.Collections;

public class AudioOneShot : MonoBehaviour {

    public AudioClip audioClip;

    private AudioSource audioSource;
    private bool hasAudioPlayed;
    private bool isPlayerColliding;

	// Use this for initialization
	void Start () {

        audioSource = GetComponent<AudioSource>();
        hasAudioPlayed = false;

	}

    void OnCollisionEnter(Collision col)
    {

        if (col.gameObject.tag.Equals("Player"))
        {
            isPlayerColliding = true;
        }
    
    }
    void OnCollisionExit(Collision col)
    {
        if (col.gameObject.tag.Equals("Player"))
        {
            isPlayerColliding = false;
        }


    }

	// Update is called once per frame
	void Update () {

        if (isPlayerColliding && !hasAudioPlayed)
        {
            audioSource.PlayOneShot(audioClip);
            hasAudioPlayed = true;
        }
	
	}
}
