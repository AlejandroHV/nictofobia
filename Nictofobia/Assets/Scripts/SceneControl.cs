﻿using UnityEngine;
using System.Collections;

public class SceneControl : MonoBehaviour {


    public AudioClip playerGroaming;

    private Animator playerAnimator;
    private bool Finish;
    

	// Use this for initialization
    void Awake()
    {
        
        playerAnimator = GetComponent<Animator>();
    
    }
    
    void Start () {
        playerAnimator.SetBool("EndBeggining", true);
        //RenderSettings.fog = true;
        //RenderSettings.fogColor = new Color(0, 0, 0, 1);
        //RenderSettings.fogDensity = 100;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void StartTransition()
    {
    
        //playerAnimator.SetBool("EndBeggining", true);
        //StopCoroutine("Fade");
    }
    public bool StartLevel()
    {
        
        //StartCoroutine("Fade");
        
        playerAnimator.SetBool("EndBeggining", true);
        RenderSettings.fog = false;
        //StopCoroutine("Fade");
        return true;
    
    }
    IEnumerator Fade()
    {
        
        for (float f = 100f; f >= 1; f -=5f)
        {
            
            RenderSettings.fogDensity = f;

            yield return null;
        }
        yield return new WaitForSeconds(1f);
        RenderSettings.fog = false;
    }
}
