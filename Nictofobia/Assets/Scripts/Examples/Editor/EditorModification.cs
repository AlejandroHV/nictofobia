﻿using UnityEngine;
using System.Collections;
using UnityEditor;

//DataAnnotation for editing the editor.
[CustomEditor(typeof(EditEditorEx))]
public class EditorModification : Editor {

    public override void OnInspectorGUI()
    {
        EditEditorEx script = target as EditEditorEx;
        EditorGUILayout.LabelField("ExampleLabel",EditorStyles.colorField);

        //Generate a horizontal Panel 
        EditorGUILayout.BeginHorizontal();
        script.intVar = EditorGUILayout.IntField("Integer field", script.intVar);
        EditorGUILayout.Space();
        script.floatVar = EditorGUILayout.FloatField("Float Field", script.floatVar);

        EditorGUILayout.EndHorizontal();
        script.enumVar =(DaEnum) EditorGUILayout.EnumPopup("Enumeration", script.enumVar);

        EditorGUILayout.ObjectField("Object", script.goVar, typeof(GameObject), false);



        //EditorGUILayout 
        // Organizacion 
            // Organizacion vertical
        //      Organizacion Horizontal. 
        //Widgets
        //      Campos.
        //      
    }
}
