﻿using UnityEngine;
using System.Collections.Generic;

public class InventaryController : MonoBehaviour
{

    Component halo;
    public GameObject lanterGO;
    public GameObject weaponGO;
    public GameObject bookGO;
    public Transform handTransform;
    
    private int actualItemIndex = 0;
    #region Methods


    public static void AddToInventary(Enums.PickeableItemsTag itemEnum)
    {

        Item findedItem = GameController.InventoryItems.Find(i => i.ItemEnum == itemEnum);
        if (findedItem != null)
        {
            findedItem.ItemCount++;
        }
        else
        {
            //Item item = new Item(itemEnum, go);
            //GameController.InventoryItems.Add(item);
        }
    }

    public static bool CheckIsPickeableObject(string objectTag)
    {
        bool isPickeable = false;
        Enums.PickeableItemsTag tag = GameController.GetPickeableString(objectTag);
        if (tag != Enums.PickeableItemsTag.None)

            isPickeable = true;

        return isPickeable;

    }

    #endregion Methods


    #region Events

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        CheckInventaryKey();
        ExploreInventory();


    }

    public void OpenInventary()
    {
        actualItemIndex = -1;

        GameController.IsInventaryOpen = true;
        GameController.freezePlayerMovement = GameController.IsInventaryOpen;
        GameController.freezeCameraMovement = GameController.IsInventaryOpen;

        ChangeInventoryItemsRendererState(GameController.IsInventaryOpen);


    }

    private void CloseInventory()
    {
        GameController.IsInventaryOpen = false;
        GameController.freezePlayerMovement = GameController.IsInventaryOpen;
        GameController.freezeCameraMovement = GameController.IsInventaryOpen;
        if (actualItemIndex != -1)
        {
            ChangeItemHaloEnableState(actualItemIndex, GameController.IsInventaryOpen);

        }

        ChangeInventoryItemsRendererState(GameController.IsInventaryOpen);

    }

    private void CheckInventaryKey()
    {

        if (Input.GetButtonDown("Inventary") && !GameController.IsInventaryOpen)
        {

            OpenInventary();

        }
        else if (Input.GetButtonDown("Inventary") && GameController.IsInventaryOpen)
        {
            CloseInventory();

        }

    }

    private void ExploreInventory()
    {

        if (GameController.IsInventaryOpen)
        {

            if (Input.GetKeyDown(KeyCode.D))
            {
                if (GameController.InventoryItems.Count > 0)
                    MoveForwardInInventary();

            }
            if (Input.GetKeyDown(KeyCode.A))
            {
                if (GameController.InventoryItems.Count > 0)
                    MoveBackWardInInventary();
            }
            if (Input.GetButtonDown("Action"))
            {
                if (GameController.InventoryItems[actualItemIndex].ItemEnum == Enums.PickeableItemsTag.Readable)
                {
                    
                }
                else
                {
                    if (GameController.ActualItemInHand == null)

                        SelectPickeableItem();

                    else
                    {
                        SaveActualItemInInventary();
                        SelectPickeableItem();

                    }
                }

            }

        }
        else if (Input.GetButtonDown("DropItem"))
        {
            SaveActualItemInInventary();
        }
    }

    private void ChangeItemHaloEnableState(int itemIndex, bool enabledValue)
    {
        Item element = GameController.InventoryItems[itemIndex];
        GameObject itemGO = GetInventaryGameObjectByEnum(element.ItemEnum);
        Component haloComponent = itemGO.GetComponent("Halo");

        if (haloComponent != null)
        {
            Behaviour haloBeha = (Behaviour)haloComponent;
            haloBeha.enabled = enabledValue;

            //haloComponent.GetType().GetProperty("enabled").SetValue(haloComponent, true, null);
            //if (!itemGO.GetComponent("halo").GetType().GetProperty("enabled").GetValue(halo, null).ToString().Equals("True"))
            //{
            //haloComponent.GetType().GetProperty("enabled").SetValue(haloComponent, true, null);
            //}
        }
    }

    /// <summary>
    /// Enable or disable the renderer component of the elements picked by the player.
    /// </summary>
    /// <param name="enableRenderer">Indicates the state of the renderer</param>
    private void ChangeInventoryItemsRendererState(bool enableRenderer)
    {
        foreach (var item in GameController.InventoryItems)
        {
            switch (item.ItemEnum)
            {
                case Enums.PickeableItemsTag.Weapon:
                    var rendererElements = weaponGO.GetComponentsInChildren<MeshRenderer>();
                    foreach (var render in rendererElements)
                    {
                        render.enabled = enableRenderer;
                    }
                    break;
                case Enums.PickeableItemsTag.Lantern:
                    var lanterRenderes = lanterGO.GetComponentsInChildren<MeshRenderer>();
                    foreach (var render in lanterRenderes)
                    {
                        render.enabled = enableRenderer;
                    }
                    break;
                case Enums.PickeableItemsTag.Readable:
                    var bookRenderers = bookGO.GetComponentsInChildren<MeshRenderer>();
                    foreach (var render in bookRenderers)
                    {
                        render.enabled = enableRenderer;
                    }
                    break;
                case Enums.PickeableItemsTag.Pistol:
                    break;
                case Enums.PickeableItemsTag.Vengals:
                    break;
                case Enums.PickeableItemsTag.Knife:
                    break;
                case Enums.PickeableItemsTag.None:
                    break;
                default:
                    break;
            }
        }

    }


    private GameObject GetInventaryGameObjectByEnum(Enums.PickeableItemsTag tag)
    {
        GameObject itemGO = null;
        switch (tag)
        {
            case Enums.PickeableItemsTag.Weapon:
                itemGO = weaponGO;
                break;
            case Enums.PickeableItemsTag.Lantern:
                itemGO = lanterGO;
                break;
            case Enums.PickeableItemsTag.Pistol:
                break;
            case Enums.PickeableItemsTag.Vengals:
                break;
            case Enums.PickeableItemsTag.Knife:
                break;
            case Enums.PickeableItemsTag.Readable:
                itemGO = bookGO;
                break;
            case Enums.PickeableItemsTag.None:
                break;
            default:
                break;
        }
        return itemGO;

    }

    /// <summary>
    /// Advance one position forward in the inventory and activated the halo in the actual item and deactivates the halo of the one before it.
    /// </summary>
    private void MoveForwardInInventary()
    {
        actualItemIndex++;
        if (actualItemIndex < GameController.InventoryItems.Count)
        {
            ChangeItemHaloEnableState(actualItemIndex, true);
            if (actualItemIndex > 0)
                ChangeItemHaloEnableState(actualItemIndex - 1, false);
        }
        else if (actualItemIndex >= GameController.InventoryItems.Count)
        {
            ChangeItemHaloEnableState(GameController.InventoryItems.Count - 1, false);
            actualItemIndex = 0;
            ChangeItemHaloEnableState(actualItemIndex, true);
        }
    }

    private void MoveBackWardInInventary()
    {
        actualItemIndex--;
        if (actualItemIndex < GameController.InventoryItems.Count && actualItemIndex >= 0)
        {
            ChangeItemHaloEnableState(actualItemIndex, true);
            if (actualItemIndex >= 0)
                ChangeItemHaloEnableState(actualItemIndex + 1, false);
        }
        else if (actualItemIndex < 0)
        {
            ChangeItemHaloEnableState(0, false);
            actualItemIndex = GameController.InventoryItems.Count - 1;
            ChangeItemHaloEnableState(actualItemIndex, true);
        }

    }

    private void SelectReadableItem()
    {

    }

    private void SelectPickeableItem()
    {


        ChangeItemHaloEnableState(actualItemIndex, false);
        GameController.ActualItemInHand = GameController.InventoryItems[actualItemIndex];
        GameObject itemGO = GetInventaryGameObjectByEnum(GameController.ActualItemInHand.ItemEnum);
        GameObject instantiatedGO = Instantiate(itemGO);
        instantiatedGO.transform.position = handTransform.position;
        instantiatedGO.transform.SetParent(handTransform);
        GameController.ActualItemInHand.ItemGO = instantiatedGO;

        CloseInventory();
        GameController.InventoryItems.RemoveAt(actualItemIndex);
    }

    private void SaveActualItemInInventary()
    {
        if (GameController.ActualItemInHand != null)
        {
            AddToInventary(GameController.ActualItemInHand.ItemEnum);
            Destroy(GameController.ActualItemInHand.ItemGO);
            GameController.ActualItemInHand = null;
        }
    }




    #endregion Events


}

