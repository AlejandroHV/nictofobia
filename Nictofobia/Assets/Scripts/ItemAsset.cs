﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu]
public class ItemAsset : ScriptableObject {

    public string ItemName; 

    public string ItemDescription;


}
