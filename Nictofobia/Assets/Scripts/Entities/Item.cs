﻿using UnityEngine;
using System.Collections;

/// <summary>
/// The elements picked in the map, and saved in the inventory.
/// </summary>
public class Item
{
    private string _itemName;
    private string _itemDescription;
    private Enums.PickeableItemsTag _itemEnum;
    private int _itemCount;
    private GameObject _itemGO;


    public string ItemDescription
    {
        get { return _itemDescription; }
        set { _itemDescription = value; }
    }

    public string ItemName
    {
        get { return _itemName; }
        set { _itemName = value; }
    }
    public Enums.PickeableItemsTag ItemEnum
    {
        get { return _itemEnum; }
        set { _itemEnum = value; }
    }

    public int ItemCount
    {
        get { return _itemCount; }
        set { _itemCount = value; }
    }
    public GameObject ItemGO
    {
        get { return _itemGO; }
        set { _itemGO = value; }
    }


    public Item() { }

    public Item(Enums.PickeableItemsTag tag, int itemCount)
    {
        ItemEnum = tag;
        ItemCount = itemCount;
    }

    public Item(Enums.PickeableItemsTag tag, GameObject itemGO)
    {
        this.ItemEnum = tag;
        this.ItemCount = 1;
        this.ItemGO = itemGO;
    }

}
