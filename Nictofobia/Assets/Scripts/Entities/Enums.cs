﻿using UnityEngine;
using System.Collections;

public class Enums
{

    public enum PlayerState
    {
        Moving,
        Inventary
    }

    public enum PickeableItemsTag
    {
        Readable,
        Weapon,
        Lantern,
        Pistol,
        Vengals,
        Knife,
        None
    }

    public enum ReadableItems
    {
        Book,
        Picture,

    }
    public enum FearStates
    {
        SinMiedo,
        MuyPocoMiedo,
        PocoMiedo,
        Miedo,
        MuchoMiedo,
        MuertoDelMiedo
    }

    public enum FriendlyFishState
    {
        WANDER,
        RUNAWAY,
        EAT,
    }

    public enum AgressiveFishState
    {
        WANDER,
        ATTACK,
        COMER
    }

    public enum CameraStates
    {
        PLAYER,
        GENERALINVENTORY,
        BOOKINVENTORY
    }

    public enum GameStates
    {
        SCAPE,
        PLAYING,
        INVENTORY,
        WON,
        DEATH,
    }
}
