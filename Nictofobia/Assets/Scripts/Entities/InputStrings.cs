﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Entities
{
    public class GameKeysStrings
    {
        public struct Inputs
        {
            public const string Inventary = "Inventary";
            public const string Cancel = "Cancel";
            public const string Action = "Action";
            public const string DropItem = "DropItem";
            public const string Tank = "Tank";
            public const string Run = "Run";
            public const string Right = "Right";
            public const string Left = "Left";
            
                
        }
        public struct Levels
        {
            public const string Dream = "Dream";
            public const string MainMenu = "MainMenu";
            public const string InicialCueva = "Main";
        
        }
    }
}
