﻿using UnityEngine;
using System.Collections;

public class NivelNervios : MonoBehaviour 
{
	//variables de nervios
	private Enums.FearStates currentFearSate;
	
	//variables de respiración
	private float velRespiracion;
	
	
	void Start () 
	{
		GameController.fearLevel = 0f;
		currentFearSate = Enums.FearStates.SinMiedo;
		//velRespiracion = 1.0f * Time.deltaTime;
		GameController.tanqueOxigeno  = 100.0f;
	}
	
	// Update is called once per frame
	void Update () 
	{
		GameController.tanqueOxigeno -= velRespiracion;
		
		if (GameController.tanqueOxigeno > 0)
		{
			// cambio entre niveles de forma manual (alteraciones en el miedo)
			if 		(GameController.fearLevel>=0 && GameController.fearLevel<20)	{currentFearSate = Enums.FearStates.SinMiedo;}
			else if (GameController.fearLevel>=20 && GameController.fearLevel<40) 	{currentFearSate = Enums.FearStates.MuyPocoMiedo;}
			else if (GameController.fearLevel>=40 && GameController.fearLevel<60) 	{currentFearSate = Enums.FearStates.PocoMiedo;}
			else if (GameController.fearLevel>=60 && GameController.fearLevel<80) 	{currentFearSate = Enums.FearStates.Miedo;}
			else if (GameController.fearLevel>=80 && GameController.fearLevel<100) 	{currentFearSate = Enums.FearStates.MuchoMiedo;}
			else if (GameController.fearLevel >= 100) 								{currentFearSate = Enums.FearStates.MuertoDelMiedo;}	
			
			// niveles de mied Switch
			switch (currentFearSate)
			{
				case Enums.FearStates.SinMiedo:
					Breathe_0 ();
				break;
				case Enums.FearStates.MuyPocoMiedo:
					Breathe_1 ();
				break;
				case Enums.FearStates.PocoMiedo:
					Breathe_2 ();
				break;
				case Enums.FearStates.Miedo:
					Breathe_3 ();
				break;
				case Enums.FearStates.MuchoMiedo:
					Breathe_4 ();
				break;
				case Enums.FearStates.MuertoDelMiedo:
					Breathe_5 ();
				break;
				default:
				break;
			}
					
			// provisional subir y bajar nervios manualmente
			if (Input.GetKeyDown(KeyCode.M)) 		{GameController.fearLevel += 5;}
			else if (Input.GetKeyDown(KeyCode.N)) 	{GameController.fearLevel -= 5;}
			
			//subir y bajar nervios según luz
			if ((GameController.fearLevel >= 0) && (GameController.fearLevel <= 100)) 
			//if (GameController.fearLevel <= 100)
			{
				if (GameController.lightOn == false)
				{
					GameController.fearLevel += 1f * Time.deltaTime;
				}
				else if (GameController.lightOn == true) 
				{
					GameController.fearLevel -= 2f * Time.deltaTime; 	
				}
			}
		}
		else if (GameController.tanqueOxigeno <= 0)
		{
			currentFearSate = Enums.FearStates.SinMiedo;
		}
		// cambio automático entre niveles
		
		//Debug.Log ("Tanque se descarga a: " + velRespiracion);
	}
	
	void Breathe_0 () 
	{
		velRespiracion = 0.05f * Time.deltaTime;
		Debug.Log ("Respiración en velocidad 0");
	}
	
	void Breathe_1 () 
	{
		velRespiracion = 0.5f * Time.deltaTime;
		Debug.Log ("Respiración en velocidad 1" );
	}	
	
	void Breathe_2 () 
	{
		velRespiracion = 1.0f * Time.deltaTime;
		Debug.Log ("Respiración en velocidad 2");
	}
	
	void Breathe_3 () 
	{ 
		velRespiracion = 2.0f * Time.deltaTime;
		//Debug.Log ("Respiración en velocidad 3");
	}
	
	void Breathe_4 () 
	{
		velRespiracion = 4.0f * Time.deltaTime;
		//Debug.Log ("Respiración en velocidad 4");
	}
	
	void Breathe_5 () 
	{
		velRespiracion = 8.0f * Time.deltaTime;
		//Debug.Log ("Respiración en velocidad 5");
	}
}
