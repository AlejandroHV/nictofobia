﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Entities;

public class PickeableObject : MonoBehaviour {

    public Enums.PickeableItemsTag itemType;
    public ItemAsset itemInfo;

    private string itemName;
    private string itemDescription;
    private bool IsPlayerColliding = false;

    


    #region Events




    void OnCollisionEnter(Collision collision)
    {
        

    }
    void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            IsPlayerColliding = true;
        }


    }



    void OnCollisionExit(Collision collision)
    {
        

    }
    // Use this for initialization
    void Start()
    {
        itemName = itemInfo.ItemName;
        itemDescription = itemInfo.ItemDescription;
    }

    // Update is called once per frame
    void Update()
    {

        if (IsPlayerColliding)
        {
            
            if (Input.GetButtonDown(GameKeysStrings.Inputs.Action))
            {
                Item item = new Item();
                item.ItemGO = gameObject;
                item.ItemEnum = itemType;
                item.ItemName = itemName;
                item.ItemDescription = itemDescription;
                InventoryTest.AddToInventary(item,itemType);
                IsPlayerColliding = false;
            }
        }
    }

    #endregion Events


    #region Methods


    #endregion Methods

}
