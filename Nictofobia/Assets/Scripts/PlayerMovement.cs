﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{

    public float speed;
    public float jumpDistance;
    public float runSpeed;
    public float onGroungAngular;
    public float playerHeight;
    public float onWaterGravityMultiplier;
    public int onWaterMoveDelay;
    public float onWaterSpeed;
    public float onWaterAngular;
    public float onWaterRunDelay;
    public AudioClip audioBubbling;
    public AudioClip audioStep;


    private AudioSource audioSource;

    private bool hasJumped = true;
    private bool canSwim;
    private int swimmingCounter;
    private Rigidbody rb = null;
    private Vector3 movement;
    private float camRayLenght = 100f;
    private bool isGrounded;
    private bool jump;
    private bool GoingOutWater;
    private bool GoingInWater;
    private bool move = false;
    //private Animator animator;


    #region Events

    void Awake()
    {
        swimmingCounter = onWaterMoveDelay;
        //animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();

    }

    // Use this for initialization
    void Start()
    {

        // Bit Shift. Convert an Layer mask to a byte. 
        // To detect the floor it's better throw the collider and some layers.

        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {


        Rotate();


    }
    void OnCollisionStay(Collision col)
    {

        if (IsObjectGrounded(col.gameObject.tag))
        {
            isGrounded = true;
        }
    }

    void FixedUpdate()
    {
        CheckWaterState();
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        bool runButton = Input.GetButton("Run");

        if (Input.GetButtonDown("Jump"))
        {
            jump = true;
            hasJumped = true;
        }
        else
            jump = false;

        if (hasJumped && isGrounded)
        {
            hasJumped = false;
        }

        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
            move = true;
        else
            move = false;

        if (!GameController.freezePlayerMovement)
            Move(horizontal, vertical, runButton);


        UpdateAnimator();


    }


    void OnCollisionExit(Collision col)
    {
        if (IsObjectGrounded(col.gameObject.tag))
        {
            isGrounded = false;
        }
    }
    

    #endregion Events


    #region Methods


    void UpdateAnimator()
    {

        //animator.SetFloat("Velocity", rb.velocity.magnitude);
        //animator.SetBool("Jump", hasJumped);
        //animator.SetBool("Grounded", isGrounded);
        //animator.SetBool("InWater", GameController.IsInWater);


    }


    void Move(float horizontal, float vertical, bool run)
    {

        movement = (Camera.main.transform.forward * vertical) + (Camera.main.transform.right * horizontal);
        if (isGrounded && jump)
            movement.y =  jumpDistance;

        else if (!GameController.IsInWater && !jump)
            movement.y = 0;



        if (GameController.IsInWater)
        {

            if (swimmingCounter == onWaterMoveDelay  && !run)
                canSwim = true;
            else if(swimmingCounter == onWaterRunDelay  && run)
                canSwim = true;
            else
                swimmingCounter++;

            if (canSwim && move)
            {

                rb.AddForce(movement * onWaterSpeed, ForceMode.Impulse);

                swimmingCounter = 0;
                audioSource.PlayOneShot(audioBubbling);
                canSwim = false;
            }


        }
        else
        {
            //audioSource.PlayOneShot(audioStep);
            if (run)
                movement = movement * runSpeed;
            else
                movement *= speed;
            
            if (move || jump)
                rb.velocity = movement;
            
        }

    }

    void Rotate()
    {
        Vector3 cameraEuler = Camera.main.transform.rotation.eulerAngles;
        //if (!GameController.IsInWater)
        //{
        cameraEuler.x = 0;
        //}

        cameraEuler.z = 0;

        transform.rotation = Quaternion.Euler(cameraEuler);


    }

    private void CheckWaterState()
    {
        if (GameController.IsInWater)
        {
            rb.useGravity = false;
            rb.angularDrag = onWaterAngular;
            Vector3 hundir = -Camera.main.transform.up * onWaterGravityMultiplier;
            rb.AddForce(hundir);
            audioSource.clip = audioBubbling;
            //audioSource.Play();
        }
        else
        {
            rb.angularDrag = onGroungAngular;
            rb.useGravity = true;
            //audioSource.Stop();
        }
    }

    public static bool IsObjectGrounded(string layerTag)
    {
        bool grounded = false;

        if (GameController.GroundLayerTags.Contains(layerTag))
        {
            grounded = true;
        }
        return grounded;
    }

    #endregion Methods


}
