﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Assets.Scripts.Entities;

public class InventoryTest : MonoBehaviour
{

    #region Propierties
    public List<GameObject> inventoryPoints;
    public Vector3 itemRotation;
    public float rotationPercentage;
    public Transform handTransform;
    public Text nameText;
    public Text descriptionText;
    public GameObject inventoryPanel;

    private static List<GameObject> Points = new List<GameObject>();
    private static List<Item> picketItems = new List<Item>();
    private int actualIndex = -1;

    #endregion Propierties

    #region Events

    //TO DO: BOOK READING, UI, ANIMATIONS. 


    // TO DO: 


    void Awake()
    {


        MergePoints();

    }
    // Use this for initialization
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {

        CheckInput();
        if (!GameController.IsInventaryOpen)
        {
            actualIndex = 0;
            RefreshItemsPosition();
            

        }
        else
        {
            
        }
        RefreshUI();
    }

    #endregion Events


    #region Methods

    private void RefreshUI()
    {
        if (GameController.CameraActualState == Enums.CameraStates.GENERALINVENTORY)
        {
            if (actualIndex >= 0)
            {
                if (GameController.InventoryItems.Count > 0)
                {
                    nameText.text = GameController.InventoryItems[actualIndex].ItemName;
                    descriptionText.text = GameController.InventoryItems[actualIndex].ItemDescription;
                }

            }

        }

    }

    public static void AddToInventary(Item iGo, Enums.PickeableItemsTag itemEnum)
    {

        iGo.ItemGO.GetComponent<PickeableObject>().enabled = false;
        Item findedItem = GameController.InventoryItems.Find(i => i.ItemEnum == itemEnum);
        if (findedItem != null)
        {
            findedItem.ItemCount++;
            if (findedItem.ItemEnum == Enums.PickeableItemsTag.Readable)
            {

                GameController.ReadeableItems.Add(iGo);
                BookInventoryManager.SaveInInventory(iGo.ItemGO);
            }
            else
            {
                Destroy(iGo.ItemGO);
            }
        }
        else
        {
            iGo.ItemGO.GetComponent<BoxCollider>().enabled = false;


            if (itemEnum == Enums.PickeableItemsTag.Readable)
            {
                GameObject defaultReadeableItem = GameObject.FindGameObjectWithTag("DEFAULTBOOK");
                var values = defaultReadeableItem.GetComponent<PickeableObject>();
                Item defaultItem = new Item();
                defaultItem.ItemCount = 0;
                defaultItem.ItemDescription = values.itemInfo.ItemDescription;
                defaultItem.ItemName = values.itemInfo.ItemName;
                defaultItem.ItemGO = defaultReadeableItem;
                defaultItem.ItemEnum = Enums.PickeableItemsTag.Readable;

                GameController.InventoryItems.Add(defaultItem);
                SaveInInventory(defaultItem.ItemGO);

                GameController.ReadeableItems.Add(iGo);
                BookInventoryManager.SaveInInventory(iGo.ItemGO);
            }
            else
            {
                GameController.InventoryItems.Add(iGo);
                SaveInInventory(iGo.ItemGO);
            }
        }

    }

    void CheckInput()
    {

        if (Input.GetButtonDown(GameKeysStrings.Inputs.Inventary) && !GameController.IsInventaryOpen && GameController.ShowInteraction)
        {

            OpenInventary();

        }
        else if (Input.GetButtonDown(GameKeysStrings.Inputs.Inventary) && GameController.IsInventaryOpen)
        {
            CloseInventory();

        }
        if (GameController.CameraActualState == Enums.CameraStates.GENERALINVENTORY && GameController.InventoryItems.Count > 0)
        {

            if (Input.GetButtonDown(GameKeysStrings.Inputs.Right))
            {

                MoveForward();
            }

            if (Input.GetButtonDown(GameKeysStrings.Inputs.Left))
            {
                MoveBackward();
            }
            if (Input.GetButtonDown(GameKeysStrings.Inputs.Action))
            {
                if (GameController.InventoryItems[actualIndex].ItemEnum == Enums.PickeableItemsTag.Readable)
                {
                    OpenBookInventory();
                }
                else
                {
                    if (GameController.ActualItemInHand == null)

                        SelectPickeableItem();

                    else
                    {
                        SaveActualItemInInventary();
                        SelectPickeableItem();

                    }
                }
            }
        }
        if (Input.GetButtonDown(GameKeysStrings.Inputs.DropItem))
        {
            SaveActualItemInInventary();
        }

    }

    private void SaveActualItemInInventary()
    {
        if (GameController.ActualItemInHand != null)
        {
            AddToInventary(GameController.ActualItemInHand, GameController.ActualItemInHand.ItemEnum);
            GameController.ActualItemInHand = null;
        }
    }

    private void SelectPickeableItem()
    {
        GameController.ActualItemInHand = GameController.InventoryItems[actualIndex];
        GameController.InventoryItems[actualIndex].ItemGO.transform.position = handTransform.position;
        GameController.InventoryItems[actualIndex].ItemGO.transform.SetParent(handTransform);
        GameController.InventoryItems[actualIndex].ItemGO.transform.rotation = handTransform.rotation;
        GameController.InventoryItems.RemoveAt(actualIndex);
        CloseInventory();
    }

    private void MoveForward()
    {
        float difference = ((float)actualIndex + 1) / ((float)GameController.InventoryItems.Count - 1);
        if (difference > 1)
            actualIndex = 0;
        else
            actualIndex++;

        float finalRotation = actualIndex * -rotationPercentage;
        Quaternion rot = Quaternion.Euler(Vector3.right * finalRotation);
        transform.localRotation = rot;

    }

    private void MoveBackward()
    {
        float difference = ((float)actualIndex - 1) / ((float)GameController.InventoryItems.Count - 1);
        if (difference < 0)
            actualIndex = GameController.InventoryItems.Count - 1;
        else
            actualIndex--;

        float finalRotation = actualIndex * -rotationPercentage;
        Quaternion rot = Quaternion.Euler(Vector3.right * finalRotation);
        transform.localRotation = rot;
    }

    private static void SaveInInventory(GameObject item)
    {
        GameObject point = Points[GameController.InventoryItems.Count - 1];
        item.transform.position = point.transform.position;
        item.transform.localRotation = Quaternion.Euler(45, 0, 0);
        item.transform.SetParent(point.transform);

    }

    private void OpenBookInventory()
    {

        GameController.CameraActualState = Enums.CameraStates.BOOKINVENTORY;


    }

    void OpenInventary()
    {
        actualIndex = 0;
        GameController.CameraActualState = Enums.CameraStates.GENERALINVENTORY;
        GameController.actualGameState = Enums.GameStates.INVENTORY;
        GameController.IsInventaryOpen = true;
        GameController.freezePlayerMovement = GameController.IsInventaryOpen;
        GameController.freezeCameraMovement = GameController.IsInventaryOpen;

    }

    void CloseInventory()
    {
        GameController.actualGameState = Enums.GameStates.PLAYING;
        GameController.IsInventaryOpen = false;
        GameController.CameraActualState = Enums.CameraStates.PLAYER;
        GameController.freezePlayerMovement = GameController.IsInventaryOpen;
        GameController.freezeCameraMovement = GameController.IsInventaryOpen;
        RefreshItemsPosition();
    }

    private void RefreshItemsPosition()
    {

        for (int i = 0; i < GameController.InventoryItems.Count; i++)
        {
            GameController.InventoryItems[i].ItemGO.transform.position = Points[i].transform.position;
            GameController.InventoryItems[i].ItemGO.transform.SetParent(Points[i].transform);
        }

    }


    private void MergePoints()
    {

        foreach (var item in inventoryPoints)
        {
            Points.Add(item);
        }
    }
    #endregion
}
